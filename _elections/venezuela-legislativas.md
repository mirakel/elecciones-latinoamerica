---
layout: election
title: Elección de la Asamblea Nacional en Venezuela
type: Legislativas
country: Venezuela
date_election: 2020-12-06
---
Esa elección buscará renovar la Asamblea Nacional (AN), de mayoría opositora, que hasta enero próximo lidera Juan Guaidó. Se trata de una institución que, desde 2016, se encuentra en desacato por decisión del Tribunal Supremo de Justicia (TSJ), tras incumplir órdenes de ese organismo.