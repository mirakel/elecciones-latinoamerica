---
layout: election
title: Elecciones presidenciales en Bolivia
type: Presidenciales
country: Bolivia
date_election: 2020-05-03
---
La elección se la disputará un candidato del partido Movimiento Al Socialismo (MAS) exministro de Economía Luis Arce, el líder del denominado “movimiento cívico” Fernando Camacho, y el expresidente y segundo en las votaciones del pasado mes de octubre, Carlos Mesa. El listado definitivo de candidatos será divulgado el próximo 3 de febrero.

Los bolivianos, además, deberán elegir, para el período 2020-2025, nuevamente a los 36 senadores y 130 diputados de la Asamblea Legislativa Plurinacional (ALP), órgano que en la actualidad está dominado por legisladores del MAS: 69 % en el Senado y 68 % en Diputados.