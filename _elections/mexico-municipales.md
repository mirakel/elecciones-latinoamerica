---
layout: election
title: Elecciones en Hidalgo y Coahuila
type: Regionales y municipales
country: México
date_election: 2020-06-07
---
En México, habrá elecciones en dos de las 32 entidades federativas el 7 de junio de 2020. De acuerdo al Instituto Nacional Electoral (INE), en Hidalgo se elegirán los titulares de 84 ayuntamientos; mientras, en el estado de Coahuila se renovará el congreso local, de 25 diputados.