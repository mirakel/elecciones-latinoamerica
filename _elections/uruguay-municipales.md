---
layout: election
title: Elecciones departamentales y municipales en Uruguay
type: Regionales y municipales
country: Uruguay
date_election: 2020-05-10
---
En cada departamento se elegirá al intendente (primer figura del ejecutivo departamental) y 31 ediles; y un alcalde y cuatro concejales por cada una de las alcaldías. En todo el país se elegirán 19 intendentes, 589 ediles, 112 alcaldes y 448 concejales.

Los uruguayos deberán elegir 19 intendentes (primera figura del ejecutivo departamental), 589 ediles (legisladores departamentales), 112 alcaldes y 448 concejales.