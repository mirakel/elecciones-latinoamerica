---
layout: election
title: Elecciones municipales en Costa Rica
type: Regionales y municipales
country: Costa Rica
date_election: 2020-02-02
---
Ese día elegirán 6.138 cargos públicos municipales, para el próximo mandato de cuatro años, según informa el Tribunal Supremo de Elecciones (TSE).

De esa cifra, 82 son alcaldes e igual número de vicealcaldes primeros y segundos. También se votará por regidores, miembros de la Concejalías de distrito y municipios e intendentes locales.