---
layout: election
title: Elecciones generales en Guayana
type: Presidenciales y Legislativas
country: Guayana
date_election: 2020-03-02
---
Los guyaneses están convocados a las urnas para el 2 de marzo, cuando deberán elegir a 65 miembros de la Asamblea Nacional. El líder del partido que reciba la mayor cantidad de escaños en el Parlamento, se convierte en el nuevo presidente.

Hasta ahora, está como candidato David Granger, el actual mandatario, por los partidos Congreso Nacional del Pueblo (PNC) y la coalición conformada por Una Asociación para la Unidad Nacional (APNU) y la Alianza para el Cambio (AFC). El otro contendiente es el exministro de Vivienda Irfaan Ali, del opositor Partido Progresista del Pueblo (PPP).
